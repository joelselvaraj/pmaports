# Maintainer: Miles Alan <m@milesalan.com>
pkgname=sxmo-dmenu
pkgver=4.9.6
pkgrel=0
pkgdesc="Dmenu fork for Sxmo UI; supports highlight, centering, volume-key navigation and more"
url="https://git.sr.ht/~mil/sxmo-dmenu"
arch="all"
license="MIT"
makedepends="libx11-dev libxinerama-dev libxft-dev"
options="!check" # has no tests
subpackages="$pkgname-doc"
provides="dmenu"
source="$pkgname-$pkgver.tar.gz::https://git.sr.ht/~mil/sxmo-dmenu/archive/$pkgver.tar.gz"

prepare() {
	default_prepare
	sed -i -e '/CFLAGS/{s/-Os//;s/=/+=/}' \
		-e '/LDFLAGS/{s/=/+=/}' \
		"$builddir"/config.mk
}

build() {
	make X11INC=/usr/include/X11 \
		X11LIB=/usr/lib/X11 \
		FREETYPEINC=/usr/include/freetype2 \
		-C "$builddir"
}

package() {
	make DESTDIR=$pkgdir PREFIX=/usr \
		-C "$builddir" install
}

sha512sums="eaab7fe2b8d1f64f0cd2d46b06a6061e9e7d0db7800d82097d82c6f4a6055a65c19e2bf38a6c250419eb6110e1e6eefe33375f8757192bc3ae5e9b2bd3648540  sxmo-dmenu-4.9.6.tar.gz"
